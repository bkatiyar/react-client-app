const express = require('express');
const path = require('path');

const app = express();

if (process.env.NODE_ENV !== 'production') {
    const webpackConfig = require('./webpack.config.js');
    const webpack = require('webpack');
    const webpackMiddleware = require('webpack-dev-middleware');
    app.use(webpackMiddleware(webpack(webpackConfig)));
} else {
    app.use(express.static('dist'));
    app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}

app.listen(3050, () => console.log('listening on port: 3050'));
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'

const styleFilter = {
    width: '120px',        
    padding: '6px',
    borderRadius: '8px'
};

const LanguageSelector = () => {
    const { i18n } = useTranslation()
    const [filter, setFilter] = useState('');

    // Change language handler
    const changeLanguage = (event) => {
        setFilter(event.target.value);
        i18n.changeLanguage(event.target.value);               
    };

    return (
        <div>
            <select id="language" value={filter} 
                onChange={changeLanguage} style={styleFilter}>
                <option value="en">English</option>
                {/* <option value="fr">French</option> */}
                <option value="pl">Polish</option> 
            </select>
        </div>
    );
}

export default LanguageSelector;
import React from 'react';

import PerformanceAnalytics from './PerformanceAnalytics';
import EmailSummary from './EmailSummary';
import SequenceAnalytics from './SequenceAnalytics';
import TemplateAnalytics from './TemplateAnalytics';
import DayToGetReplyAnalytics  from './DayToGetReplyAnalytics';
import OpenRateAnalytics  from './OpenRateAnalytics';
import DaywiseAnalytics  from './DaywiseAnalytics';
import FollowUpwiseAnalytics from './FollowUpwiseAnalytics';
import HourwiseAnalytics  from './HourwiseAnalytics';

const Analytics = () => {
	// Use this to display sequence dropdown in graph
	const visible = {
		sequence: false,
		template: false,
		dayToGetReply: false,
		openRate: false,
		daywise: true,
		followupwise: true,
		hourwise: true
	};
	
	// List of the sequences will apprear within dropdown
	const sequences = [
		{ _id: '1', name: 'Propsal Sequence' },		
		{ _id: '2', name: 'Demo Request Sequence' },
		{ _id: '3', name: 'Payment Sequence' },
		{ _id: '4', name: 'Implementation Sequence' },
		{ _id: '5', name: 'Go Live Sequence' }
	];

    return (
        <div className="container">

			{/* <PerformanceAnalytics /> */}

			<EmailSummary />

			<div className="card-deck mt-3">
				<SequenceAnalytics 
					visible={visible.sequence} 
				/>

				<TemplateAnalytics 
					visible={visible.template} 
				/>				
			</div>
			
			<div className="card-deck mt-3">
				<DayToGetReplyAnalytics 
					visible={visible.dayToGetReply} 
				/>
			
				<OpenRateAnalytics 
					visible={visible.openRate} 
				/>
			</div>

			<div className="card-deck mt-3">
				<DaywiseAnalytics 
					visible={visible.daywise} 
					sequences={sequences} 
				/>
				
				<FollowUpwiseAnalytics 
					visible={visible.followupwise} 
					sequences={sequences} 
				/>
			</div>

			<HourwiseAnalytics 
				visible={visible.hourwise} 
				sequences={sequences} 
			/>
        </div>        
    );
};

export default Analytics;
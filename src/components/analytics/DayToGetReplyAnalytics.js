import React from 'react';
import HighChartOptions from '../../config/HighChartOptions';
import { categoriesSequenceDayToGetReply, seriesSequenceDayToGetReply } from '../../data/Chart';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const DayToGetReplyAnalytics = () => {
    const { t, i18n } = useTranslation();
    const options = {
		...HighChartOptions, 
		xAxis: { categories: categoriesSequenceDayToGetReply }, 
		series: seriesSequenceDayToGetReply
    };
    
    return (        
        <Chart 
            options= {options} 
            title={t('graph.sequence.day.to.get.reply.label')}
            chartContainer="partial-width" 
        />                
    );
};

export default DayToGetReplyAnalytics;
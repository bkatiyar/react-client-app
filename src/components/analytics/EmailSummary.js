import React from 'react';
import { useTranslation } from 'react-i18next';

const EmailSummary = () => {
    const { t, i18n } = useTranslation();

    const email = {
        sent: 400,
        opened: 220,
        replied: 100,
        bounced: 30
    };

	return (
        <div className="card" style={{ flexDirection: 'row'}}>
            <div className="card-body text-success text-white col-sm-3">
                {email.sent}<br /> {t('graph.legend.sent.label')}
            </div>
            <div className="card-body text-primary text-white col-sm-3">
                {email.opened}<br /> {t('graph.legend.opened.label')}
            </div>
            <div className="card-body text-warning text-white col-sm-3">
                {email.replied}<br /> {t('graph.legend.replied.label')}
            </div>
            <div className="card-body text-secondary text-white col-sm-3">
                {email.bounced}<br /> {t('graph.legend.bounced.label')}
            </div>
        </div>
    );
};

export default EmailSummary;
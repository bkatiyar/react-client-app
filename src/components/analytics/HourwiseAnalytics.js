import React, { useState, useEffect } from 'react';
import HighChartOptions from '../../config/HighChartOptions';
import { categoriesHourwise, seriesHourwise } from '../../data/Chart';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const HourwiseAnalytics = (props) => {
    const { sequences, visible } = props;
    const [ selectedSequenceId, setSelectedSequenceId ] = useState(sequences[0]._id);
    const { t, i18n } = useTranslation();
    const options = {
		...HighChartOptions, 
		xAxis: { categories: categoriesHourwise }, 
		series: seriesHourwise
    };
    
    useEffect(() => {

    }, [selectedSequenceId]);
    
    const changeSequence = (event) => {
        setSelectedSequenceId(event.target.value);
        // Need to call appropiate api based on sequence id 
    };

    return (
        <>
            <Chart 
                options= {options} 
                title={t('graph.sequence.hourwise.label')}
                selectedSequenceId={selectedSequenceId}
                sequences={sequences}
                changeSequence={changeSequence}
                visible={visible}
                chartContainer="full-width" 
            />
        </>                
    );
};

export default HourwiseAnalytics;
import React from 'react';
import HighChartOptions from '../../config/HighChartOptions';
import { categoriesSequencewiseOpenRate, seriesSequencewiseOpenRate } from '../../data/Chart';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const OpenRateAnalytics = () => {
    const { t, i18n } = useTranslation();
    const { yAxis, plotOptions, plotOptions: { series } } = HighChartOptions;

    const options = {
        ...HighChartOptions,
        yAxis: {
            ...yAxis,
            labels: {
                format: '{value}%',
            }
        },
        plotOptions: {
            ...plotOptions,
            series: {
                ...series,
                dataLabels: {
                    enabled: true,
                    format: '{y} %'
                }
            }
        }, 
		xAxis: { 
            categories: categoriesSequencewiseOpenRate 
        }, 
		series: seriesSequencewiseOpenRate
    };
    
    return (        
        <Chart 
            options= {options} 
            title={t('graph.sequence.wise.open.rate.label')}
            chartContainer="partial-width" 
        />                
    );
};

export default OpenRateAnalytics;
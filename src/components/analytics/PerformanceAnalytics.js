import React from 'react';
import HighChartBoostOptions from '../../config/HighChartBoostOptions';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const PerformanceAnalytics = () => {
    const { t, i18n } = useTranslation();
    const maxNumber = 3;

    
    /* {
        1: Performance Improvement,
        1.5: Development Needed,
        2: Strong Contributor,
        2.5: Excellent,
        3: Role Model
    }
    */

    // Prepare the data
    var data = [],
        n = 100,
        i;

    for (i = 1; i <= n; i += 1) {
        var xData = Number((Math.random() * maxNumber + 1).toFixed(2));
        var yData = Number((Math.random() * maxNumber + 1).toFixed(2));
        var fillColor, label;

        xData = xData <= maxNumber ? xData : maxNumber;
        yData = yData <= maxNumber ? yData : maxNumber;
        var sum = Number((xData + yData).toFixed(2));
        var average = sum/2;

        console.log(average);

        if (average >= 3) {
            fillColor = 'green';
            label = 'Role Model';
        } else if (average >= 2.5) {
            fillColor = 'blue';
            label = 'Excellent';
        } else if (average >= 2) {
            fillColor = 'yellow';
            label = 'Strong Contributor';
        } else if (average >= 1.5) {
            fillColor = 'orange';
            label = 'Development Needed';
        } else {
            fillColor = 'red';
            label = 'Performance Improvement';
        }

        var dataObject = {
            //name: `name${i}`,
            x: xData,
            y: yData,
            average: average,
            label: label,
            fillColor: fillColor,
            marker: { fillColor: fillColor },
            tooltip: {
                followPointer: false,
                pointFormat: '[aaaaaa, {point.x:.1f}, {point.y:.1f}]'
            }
        };

        data.push(dataObject);
    }
    
    console.log(data);

    const options = {
		...HighChartBoostOptions, 
		series: [{
            data: data
        }]
    };

    const afterRender = (chart) => {
        console.log('after render...');
    };
    
    return (
        <Chart 
            options= {options} 
            title={t('sequence.title.label')} 
        />		      
    );
};

export default PerformanceAnalytics;
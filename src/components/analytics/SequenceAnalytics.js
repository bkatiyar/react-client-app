import React from 'react';
import HighChartOptions from '../../config/HighChartOptions';
import { categoriesSequence, seriesSequence } from '../../data/Chart';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const SequenceAnalytics = () => {
    const { t, i18n } = useTranslation();
    const options = {
		...HighChartOptions, 
		xAxis: { categories: categoriesSequence }, 
		series: seriesSequence
    };
    
    return (        
        <Chart 
            options= {options} 
            title={t('graph.sequence.title.label')}
            chartContainer="partial-width" 
        />                
    );
};

export default SequenceAnalytics;
import React from 'react';
import HighChartOptions from '../../config/HighChartOptions';
import { categoriesTemplate, seriesTemplate } from '../../data/Chart';
import { useTranslation } from 'react-i18next';
import Chart from '../utility/Chart';

const TemplateAnalytics = () => {
	const { t, i18n } = useTranslation();
    const options = {
		...HighChartOptions, 
		xAxis: { categories: categoriesTemplate }, 
		series: seriesTemplate
	}; 
	
    return (
		<Chart 
			options= {options} 
			title={t('graph.template.title.label')}
			chartContainer="partial-width" 
		/>       
    );
};

export default TemplateAnalytics;
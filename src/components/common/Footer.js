import React from 'react';
import { useTranslation } from 'react-i18next';

const Footer = () => {
	const { t, i18n } = useTranslation();
	return (
		<div className="row">
			<div className="col-sm-12 text-center">{t('footer.text.label')}</div>
		</div>
	);
};

export default Footer;
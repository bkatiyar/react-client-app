import React, { useContext, useEffect, useState } from 'react';
import { withRouter, NavLink, useHistory, useLocation } from 'react-router-dom';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { useTranslation } from 'react-i18next';
import Cookie from "js-cookie";
import { ToastContainer, toast } from 'react-toastify';
import Config from '../../config/Config';
import { parseMessage } from '../../helpers/Utils';
import { postWebService } from '../../helpers/ServerUtils';
import UserContext from '../../context/UserContext';
import LanguageSelector from '../LanguageSelector';
import 'react-toastify/dist/ReactToastify.min.css';

const Header = () => {
	const { t } = useTranslation();
	const history = useHistory();
	const { state, dispatch } = useContext(UserContext);
	const { currentUser } = state || {};
	const { username, pic_thumb } = currentUser || {};
		
	useEffect(() => {
		
	}, [currentUser]);

	const logout = async (isCustomLogin = true) => {
		Cookie.set("token", null);

		const user = await dispatch({
			type: "SET_CURRENT_USER",
			payload: null
		});		

		if (!user) {
			history.push("/");
		}		
	};

	const googleLogout = async () => {
		Cookie.set("token", null);

		const user = await dispatch({
			type: "SET_CURRENT_USER",
			payload: null
		});		

		if (!user) {
			history.push("/");
		}
	};

	const notify = (message) => {
        toast.success(message);
    };

	const responseGoogle = async (response) => {
		console.log(response);
		//return false;

		const { profileObj } = response;
		const { email, googleId, imageUrl, name } = profileObj;		
		const payload = { 
			email, 
			username: name, 
			password: 'password', 
			googleId, 
			pic: imageUrl, 
			pic_thumb: imageUrl, 
			providerName: 'Google'
		};
		const headers = null;		
		const responseData = await postWebService('users/login-via-google', payload, headers);
		const { data, status, message } = responseData.data;

		if (status === 'success') {
			dispatch({ type: 'SET_CURRENT_USER', payload: data});
			Cookie.set("token", data.token);
			notify(message);
			history.push("/");
		} else {
			setError(() => parseMessage(message));
		}
	};

	return (
		<nav className="navbar navbar-expand-sm bg-primary navbar-dark">
			<div className="container-fluid">
				<ul className="navbar-nav">
					<li className="nav-item">
						<LanguageSelector />
					</li>
					<li className="nav-item">
						<NavLink to="/" exact={true} className="nav-link">{t('link.home.label')}</NavLink>
					</li>
					{currentUser && <li className="nav-item">
						<NavLink to="/analytics" className="nav-link">{t('link.analytics.label')}</NavLink>
					</li>}					
					{currentUser && <li className="nav-item">
						<NavLink to="/template" className="nav-link">{t('link.template.label')}</NavLink>
					</li>}
					{currentUser && <li className="nav-item">
						<NavLink to="/sequence" className="nav-link">{t('link.sequence.label')}</NavLink>
					</li>}
					{currentUser && <li className="nav-item">
						<NavLink to="/todo" className="nav-link">{t('link.todo.label')}</NavLink>
					</li>}
				</ul>
				{!currentUser && <ul className="nav navbar-nav navbar-right">
					<li>
						<GoogleLogin
							clientId={Config.clientId}
							render={renderProps => (
								<button className="btn btn-success" 
									onClick={renderProps.onClick} 
									disabled={renderProps.disabled}> 
									{t('button.google.login.label')} 
								</button>
							)}
							buttonText={t('button.google.login.label')}
							onSuccess={responseGoogle}
							onFailure={responseGoogle}
							cookiePolicy={'single_host_origin'}
							scope={'profile https://mail.google.com/'}
						/>
					</li>					
					<li className="nav-item">
						<NavLink to="/login" className="nav-link">
							<span className="glyphicon glyphicon-log-in"></span> {t('button.login.label')}
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink to="/register" className="nav-link">
							<span className="glyphicon glyphicon-user"></span> {t('button.signup.label')}
						</NavLink>
					</li>					
				</ul>}
				{currentUser && <ul className="nav navbar-nav navbar-right">
					<li className="nav-item">
						<NavLink to="/profile" className="nav-link">
							<img src={pic_thumb} alt="Avatar" className="avatar"
								style={{'verticalAlign': 'middle', width: '30px', height: '30px',
									borderRadius: '50%', border: '0px'}} /> {username}
						</NavLink>
					</li>
					<li className="nav-item ml-2">
						<GoogleLogout
							clientId={Config.clientId}
							render={renderProps => (
								<button className="btn btn-danger" 
									onClick={renderProps.onClick} 
									disabled={renderProps.disabled}> 
									{t('button.google.logout.label')} 
								</button>
							)}
							buttonText={t('button.google.logout.label')}
							onLogoutSuccess={googleLogout}
							scope={'profile https://mail.google.com/'}
							>
						</GoogleLogout>
						<span className="navbar-text btn" 
							onClick={logout}>{t('button.logout.label')}</span>
					</li>
				</ul>}
			</div>
			<ToastContainer position="bottom-right" autoClose={10000} />
    	</nav>
	);
};

export default withRouter(Header);

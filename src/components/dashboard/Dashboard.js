import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

const Dashboard = () => {
	const { t, i18n } = useTranslation();
	
	return (
		<div>
			{t('link.home.label')}			
		</div>
	);
};

export default Dashboard;
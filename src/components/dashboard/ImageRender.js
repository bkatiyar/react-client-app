import React, { useContext } from 'react';

import UserContext from '../../context/UserContext';
import small from '../../assets/small.jpg';
import big from '../../assets/big.jpg';
import '../../css/image_viewer.css';

const RenderImage = () => {
    const { currentUser } = useContext(UserContext);

    return (
        <div>
            <img src={small} alt="" />
            {/* <img src={big} alt="" /> */}
        </div>        
    );
};

export default RenderImage;
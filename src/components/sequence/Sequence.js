import React, { useContext, useReducer, useEffect } from 'react';
import { useAPI } from '../../helpers/Utils';
import SequenceContext from '../../context/SequenceContext';
import SequenceReducer from '../../reducers/SequenceReducer';
import SequenceForm from './SequenceForm';
import SequenceList from './SequenceList';

const EmailSequence = () => {
	const initialState = useContext(SequenceContext);
	const [state, dispatch] = useReducer(SequenceReducer, initialState);
	const emailSequences = useAPI('sequence');
	const emailTemplates = useAPI('template');
	
	useEffect(
		() => {
			dispatch({
				type: "GET_EMAIL_SEQUENCES",
				payload: emailSequences
			});
		},
		[emailSequences]
	);

	return (
			<SequenceContext.Provider value={{ state, dispatch }}>
				<SequenceForm emailTemplates={emailTemplates} />
				<SequenceList />
			</SequenceContext.Provider>
	);
};

export default EmailSequence;
import React, { useState, useContext, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import SequenceContext from '../../context/SequenceContext';
import { postWebService, putWebService } from '../../helpers/ServerUtils';
import { parseMessage } from '../../helpers/Utils';
import 'react-toastify/dist/ReactToastify.min.css';

const initialFormState = {
	name: "",
	description: ""
};
const styleCursor = {
    cursor: 'pointer'
};
const styleFilter = {
    width: '300px',        
    padding: '6px',
    marginTop: '8px',
    marginRight: '16px',
    border: 'none',
    fontSize: '17px'
};

const EmailSequenceForm = (props) => {
    const { t } = useTranslation();
    const [form, setForm] = useState(initialFormState);
    const [error, setError] = useState('');
    const [templates, setTemplates] = useState([]);
    const [isValid, setIsValid] = useState(false)
    const { emailTemplates } = props;
    const {state: { selectedEmailSequence = {} }, dispatch} = useContext(SequenceContext);
    const templateList = [{_id: '', name: 'Select Template'}, ...emailTemplates];

    useEffect(() => {
        setForm({
            name: selectedEmailSequence.name || '',
            description: selectedEmailSequence.description || ''
        });

        setTemplates(selectedEmailSequence.templates || []);
    }, [selectedEmailSequence._id]);

    const notify = (message) => {
        toast.success(message);
    };

    const cb = function ({ data, status,  message }, type) {
        if (status === 'success') {
            dispatch({ type: type, payload: data });
            notify(message);
            setForm(initialFormState);
            setTemplates([]);
        } else {
            setError(() => parseMessage(message));
		}         
    };

    const saveEmailSequence = async () => {
        const payload = Object.assign({}, form, { templates });
        const response = await postWebService(`sequence`, payload);

        cb(response.data, "ADD_EMAIL_SEQUENCE");    
    };

    const updateEmailSequence = async () => {
        const payload = Object.assign({}, form, { templates });
        const response = await putWebService(`sequence/${selectedEmailSequence._id}`, payload);
        
        cb(response.data, "UPDATE_EMAIL_SEQUENCE"); 
    };  

    const handleSubmit = event => {
        event.preventDefault();
        setError('');
        selectedEmailSequence.name ? updateEmailSequence() : saveEmailSequence();
        resetForm();
    };

    const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value
		});
	};

    const resetForm = () => {        
        dispatch({
            type: "SET_SELECTED_EMAIL_SEQUENCE",
            payload: {}
        });
        
        setForm(initialFormState);
    };

    const handleAddMoreButton = () => {
        setTemplates(
            [...templates, { template: undefined, days: undefined }]
        );
    };

    const handleRemoveButton = ({ template }) => {
        const filteredTemplates = templates.filter( item => item.template != template);
        setTemplates(filteredTemplates);
    };

    const changeHandler = (event, item, keyname) => {
        const elValue = event.target.value;
        item[keyname] = keyname === 'days' ? (elValue ? parseInt(elValue, 10) : 0) : elValue;
        isEntryValid();
    };

    const isEntryValid = () => {
        setIsValid(templates[templates.length - 1].template && templates[templates.length - 1].days != 0);
    };

    return (
        <>        
            <div className="mt-0 mb-4 border p-4">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label>{t('sequence.name.label')}:</label>
                        <input type="text" name="name" className="form-control" 
                            placeholder={t('enter.text.label')} value={form.name}
                            onChange={handleChange} />
                    </div>
                    <div className="form-group">
                        <label>{t('sequence.description.label')}:</label>
                        <input type="text" name="description" className="form-control" 
                            placeholder={t('enter.text.label')} value={form.description}
                            onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>{t('sequence.templates.label')}:</label>
                        {
                            templates.length == 0 && <div className="text-secondary small">
                                {t('template.no.label')}</div>
                        }                        
                        {
                            templates.length > 0 && templates.map((item, index)=> (
                                <div key={index}>
                                    <span >
                                        <select style={styleFilter} value={item.template}
                                            onChange={(event) => changeHandler(event, item, 'template')}>
                                            {
                                                templateList.map((option)=> (
                                                    <option key={option._id} value={option._id}>
                                                        {option.name}</option>
                                                ))
                                            }
                                        </select>
                                    </span>
                                    <span className="ml-4">
                                        <input type="number" placeholder={t('sequence.after.days.label')} 
                                            value={item.days} style={{...styleFilter, width: '110px'}}
                                            onChange={(event) => changeHandler(event, item, 'days')} />
                                    </span>
                                    <span className="ml-4 p-2 bg-danger text-white small rounded" 
                                        onClick={() => handleRemoveButton(item)}
                                        style={styleCursor}>{t('template.remove.label')}</span>
                                </div>
                            ))
                        }                        
                    </div>
                    <div className="form-group">
                        {templates.length == 0 && <span className="small text-primary mt-4 ml-2" 
                            style={styleCursor} 
                            onClick={handleAddMoreButton}>{t('template.add.label')}</span>}

                        {(templates.length > 0 && isValid) && <span className="small text-primary mt-4 ml-2" 
                            style={styleCursor} 
                            onClick={handleAddMoreButton}>{t('template.addMore.label')}</span>}
                    </div>
                    
                    {error && <div className="text-danger mb-2">{error}</div>}

                    <div className="text-right">
                        <button type="submit" className="btn btn-primary ml-2">
                            { selectedEmailSequence.name ? t('button.update.label') : t('button.save.label') }
                        </button>
                    </div>
                </form>                
            </div>                       

            <ToastContainer position="bottom-right" autoClose={10000} />
        </>
    );
};

export default EmailSequenceForm;


import React, { useState, useEffect, useContext } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import ReactTooltip from "react-tooltip";
import Moment from 'react-moment';
import Modal from 'react-bootstrap/Modal';
import { confirmAlert } from 'react-confirm-alert';
import Paginator from 'react-hooks-paginator';
import { useTranslation } from 'react-i18next';
import Common from '../../constant/Common';
import { scrollTop } from '../../helpers/Utils';
import { deleteWebService } from '../../helpers/ServerUtils';
import SequenceContext from '../../context/SequenceContext';

import 'react-confirm-alert/src/react-confirm-alert.css';
import 'react-toastify/dist/ReactToastify.min.css';

const EmailSequenceList = () => {
    const { t } = useTranslation();
    const styleCursor = {
        cursor: 'pointer'
    };
    
    // Set component state variables
    const [offset, setOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [currentData, setCurrentData] = useState([]);
    const [show, setShow] = useState(false);

    // SequenceContext use
    const { state, dispatch } = useContext(SequenceContext);
        
    // Set title based on length
    const title = typeof state.emailSequences !== "undefined" && state.emailSequences.length 
        ? `${state.emailSequences.length} ${t('sequence.title.label')}` : t('sequence.no.label');

    // Extract list data as per offset and page limit
    useEffect(() => {
        setCurrentData(state.emailSequences.slice(offset, offset + Common.PAGE_LIMIT));
    }, [offset, state.emailSequences]);
    
    // Toaster Success Message
    const notify = (message) => {
        toast.success(message);
    };
    
    // Edit Email Sequence Handler
    const editEmailSequence = (item, isPreview) => {
        // Trigger action creator
        dispatch({
            type: 'SET_SELECTED_EMAIL_SEQUENCE',
            payload: item
        });

        scrollTop();
    };

    // Delete Email Sequence Handler
    const deleteEmailSequence = async (item) => {
        // Invoke API call with appropriate url
        const response = await deleteWebService(`sequence/${item._id}`);

        // Extract API responses
        const { status,  message } = response.data;
        
        // Handle API response
        if (status === 'success') {
            dispatch({ 
                type: "DELETE_EMAIL_SEQUENCE", 
                payload: item 
            });
        }

        // Render success message
        notify(message);
    };  

    // Hide Modal
    const handleClose = () => setShow(false);

    const clickHandler = (cb, item) => {
        confirmAlert({
            title: t('sequence.delete.label'),
            message: t('confirm.sure.label'),
            buttons: [
                {
                    label: t('button.yes.label'),
                    onClick: () => cb(item)
                },
                {
                    label: t('button.yes.label'),
                    onClick: () => console.log(t('do.nothing.label'))
                }
            ]
        });
    };
            
    return (
        <div>
            <h3 className="mb-4">{title}</h3>            

            {state.emailSequences.length > 0 && <ul className="list-group">
                <li key="header" className="list-group-item d-flex justify-content-between align-items-center">
                    <span className="col-sm-10 pl-3 font-weight-bold"> {t('sequence.name.label')}</span>
                    <span className="col-sm-2"></span>
                </li>
                { 
                    currentData.map((item) => (
                        <li key={item._id} className="list-group-item d-flex justify-content-between align-items-center">
                            <span className="col-sm-10">
                                <div> {item.name} </div>
                                <div className="small text-secondary">
                                    (Created at: <Moment date={item.createdAt} format="DD-MMM-YYYY" />)
                                </div>
                            </span>

                            <span className="col-sm-2 text-right">                                
                                <span className="ml-4" data-tip={t('sequence.edit.label')}
                                    onClick={ () => editEmailSequence(item, false) }
                                    style={styleCursor}>                                    
                                    <i className="fas fa-edit text-primary"></i>
                                </span>     
                                                           
                                <span className="ml-4" data-tip={t('sequence.delete.label')}
                                    onClick={ () => clickHandler(deleteEmailSequence, item) }
                                    style={styleCursor}>
                                    <i className="fas fa-trash text-danger"></i>
                                </span>

                                <ReactTooltip />
                            </span>
                        </li>
                    ))
                }                
            </ul>}
            
            <Paginator
                totalRecords={state.emailSequences.length || 0}
                pageLimit={Common.PAGE_LIMIT}
                pageNeighbours={1}
                setOffset={setOffset}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
            />           
            
            <ToastContainer position="bottom-right" autoClose={2000} />

            <Modal size="lg" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {t('sequence.preview.label')} ({state.selectedEmailSequence.name})
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div>                        
                        { state.selectedEmailSequence.templates }
                    </div>
                </Modal.Body>
                
                {/* <Modal.Footer></Modal.Footer> */}
            </Modal>
        </div>
    );
};

export default EmailSequenceList;
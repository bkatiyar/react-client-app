import React, { useContext, useReducer, useEffect } from 'react';

import { useAPI } from '../../helpers/Utils';
import TemplateContext from '../../context/TemplateContext';
import TemplateReducer from '../../reducers/TemplateReducer';
import TemplateForm from './TemplateForm';
import TemplateList from './TemplateList';

const EmailTemplate = () => {
	const initialState = useContext(TemplateContext);
	const [state, dispatch] = useReducer(TemplateReducer, initialState);
	const emailTemplates = useAPI("template");

	useEffect(
		() => {
			dispatch({
				type: "GET_EMAIL_TEMPLATES",
				payload: emailTemplates
			});
		},
		[emailTemplates]
	);

	return (
			<TemplateContext.Provider value={{ state, dispatch }}>
			<TemplateForm />
			<TemplateList />
			</TemplateContext.Provider>
	);
};

export default EmailTemplate;
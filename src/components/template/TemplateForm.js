import React, { useState, useContext, useEffect, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { Editor } from '@tinymce/tinymce-react';
import { useTranslation } from 'react-i18next';
import TagsInput from 'react-tagsinput';

import TEMPLATE from '../../constant/Template';
import TemplateContext from '../../context/TemplateContext';
import { postWebService, putWebService } from '../../helpers/ServerUtils';
import TinyMce from '../../config/TinyMce';
import { parseMessage } from '../../helpers/Utils';

import 'react-toastify/dist/ReactToastify.min.css';
import 'react-tagsinput/react-tagsinput.css' // If using WebPack and style-loader.

const initialFormState = {
	name: "",
	description: "",
    subject: "",
    body: "",
    read_email: false,
    download_attachment: false,
    link_clicked: false,
    read_email_notification: "",
    download_attachment_notification: "",
    link_clicked_notification: "",
    tags: []
};
const checkboxes = ['read_email', 'download_attachment', 'link_clicked'];

const EmailTemplateForm = () => {
    const { t, i18n } = useTranslation();
    const [form, setForm] = useState(initialFormState);
    const [error, setError] = useState('');
    const {state: { selectedEmailTemplate = {} }, dispatch} = useContext(TemplateContext);

    // Set component references
    const inputElementOnRead = useRef(false);
    const inputElementDownloadAttahment = useRef(false);
    const inputElementClickLink = useRef(false);
    
    useEffect(() => {
        setForm({
            name: selectedEmailTemplate.name || '',
            description: selectedEmailTemplate.description || '',
            subject: selectedEmailTemplate.subject || '',
            body: selectedEmailTemplate.body || '',
            read_email: selectedEmailTemplate.on_open_email_enabled || false,            
            link_clicked: selectedEmailTemplate.on_body_link_clicked_enabled || false,
            download_attachment: selectedEmailTemplate.on_attachment_download_enabled || false,
            read_email_notification: selectedEmailTemplate.read_email_notification || '',            
            link_clicked_notification: selectedEmailTemplate.link_clicked_notification || '',
            download_attachment_notification: selectedEmailTemplate.download_attachment_notification || '',
            tags: selectedEmailTemplate.tags || []
        });

        inputElementOnRead.current.checked = selectedEmailTemplate.read_email;        
        inputElementClickLink.current.checked = selectedEmailTemplate.link_clicked;
        inputElementDownloadAttahment.current.checked = selectedEmailTemplate.download_attachment;
    }, [selectedEmailTemplate._id]);
   
    const notify = (message) => {
        toast.success(message);
    };

    const cb = function ({ data, status,  message }, type) {
        if (status === 'success') {
            dispatch({ type: type, payload: data });
            notify(message);
        } else {
            setError(() => parseMessage(message));
		}         
    };
    const buildCheckboxObject = () => {
        return { 
            read_email: inputElementOnRead.current.checked,
            link_clicked: inputElementClickLink.current.checked,
            download_attachment: inputElementDownloadAttahment.current.checked 
        };
    };
    const saveEmailTemplate = async () => {
        const payload = Object.assign({}, form, buildCheckboxObject());
        const response = await postWebService('template', payload);

        cb(response.data, "ADD_EMAIL_TEMPLATE");    
    };

    const updateEmailTemplate = async () => {
        const payload = Object.assign({}, form, buildCheckboxObject());
        const response = await putWebService(`template/${selectedEmailTemplate._id}`, payload);
        
        cb(response.data, "UPDATE_EMAIL_TEMPLATE"); 
    };  

    const handleSubmit = event => {
        event.preventDefault();
        setError('');
        selectedEmailTemplate.body ? updateEmailTemplate() : saveEmailTemplate();
        resetForm();
    };   

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: checkboxes.indexOf(event.target.name) > -1 
                ? event.target.checked : event.target.value
        });
	};

    const resetForm = () => {        
        dispatch({
            type: "SET_SELECTED_EMAIL_TEMPLATE",
            payload: {}
        });
        
        setForm(initialFormState);
    
        inputElementOnRead.current.checked = false;
        inputElementDownloadAttahment.current.checked = false;
        inputElementClickLink.current.checked = false;
    };

    const handleEditorChange = (content, editor) => {
        setForm({
			...form,
			body: content
		});
    };

    const handleTagChange = (tags) => {
        setForm({
			...form,
			tags: tags
		});
    };

    const renderTinyMceEditor = () => {
        return <Editor 
            apiKey={TinyMce.API_KEY}
            initialValue=""
            init={TinyMce.initOptions}
            onEditorChange={handleEditorChange}
            value={form.body}
        />;
    };

    return (
        <>        
            <div className="mt-0 mb-4 border p-4">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label>{t('template.name.label')}:</label>
                        <input type="text" name="name" className="form-control" 
                            placeholder={t('enter.text.label')} value={form.name}
                            onChange={handleChange} />
                    </div>

                    <div className="form-group">
                        <label>Tags</label>
                        <TagsInput placeholder={t('enter.text.label')}
                            value={form.tags} 
                            onChange={handleTagChange} />
                    </div>

                    {/* <div className="form-group">
                        <label>{t('template.description.label')}:</label>
                        <input type="text" name="description" className="form-control" 
                            placeholder={t('enter.text.label')} value={form.description}
                            onChange={handleChange} />
                    </div> */}
                    
                    {/* <div className="form-group">
                        <label>{t('template.subject.label')}:</label>
                        <input type="text" name="subject" className="form-control" 
                            placeholder={t('enter.text.label')} value={form.subject}
                            onChange={handleChange} />
                    </div> */}

                    <div className="form-group">
                        <label>{t('template.internal.notification.setting.label')}</label>
                    </div>
                    
                    <div className="form-group">
                        <div>
                            <input type="checkbox" className="mr-2" name="read_email"
                                ref={inputElementOnRead} defaultChecked={form.read_email}
                                onChange={handleChange} /> {t('template.send.notification.on.read.email')}

                            {inputElementOnRead.current.checked && <input type="text" 
                                name="read_email_notification" className="form-control" 
                                placeholder={t('enter.text.label')} 
                                value={form.read_email_notification}
                                onChange={handleChange} />}
                        </div>

                        <div className="mt-4">
                            <input type="checkbox" className="mr-2" name="link_clicked"
                                ref={inputElementClickLink} 
                                defaultChecked={form.link_clicked}
                                onChange={handleChange} /> {t('template.send.notification.on.link.clicked.in.email.body.attachment')}

                            {inputElementClickLink.current.checked && <input type="text" 
                                name="link_clicked_notification" className="form-control" 
                                placeholder={t('enter.text.label')} 
                                value={form.link_clicked_notification}
                                onChange={handleChange} />}
                        </div>  

                        <div className="mt-4">
                            <input type="checkbox" className="mr-2" name="download_attachment"
                                ref={inputElementDownloadAttahment} 
                                defaultChecked={form.download_attachment}
                                onChange={handleChange} /> {t('template.send.notification.on.download.attachment')}

                            {inputElementDownloadAttahment.current.checked && <input type="text" 
                                name="download_attachment_notification" className="form-control" 
                                placeholder={t('enter.text.label')} 
                                value={form.download_attachment_notification}
                                onChange={handleChange} />}
                        </div>                      
                    </div>

                    <div className="form-group">
                        <label>{t('template.body.label')}:</label>
                        {renderTinyMceEditor()}
                    </div>

                    {error && <div className="text-danger mb-2">{error}</div>}

                    <div className="text-right">
                        {selectedEmailTemplate && <button type="button" className="btn btn-secondary"
                            onClick={resetForm}> {t('button.reset.label')}
                        </button>}
                        <button type="submit" className="btn btn-primary ml-2">
                            { selectedEmailTemplate.name ? t('button.update.label') : t('button.save.label') }
                        </button>
                    </div>
                </form>                
            </div>                       

            <ToastContainer position="bottom-right" autoClose={10000} />
        </>
    );
};

export default EmailTemplateForm;


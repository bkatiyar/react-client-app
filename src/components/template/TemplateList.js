import React, { useState, useEffect, useContext } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import ReactTooltip from "react-tooltip";
import Moment from 'react-moment';
import Modal from 'react-bootstrap/Modal';
import ReactHtmlParser from 'react-html-parser';
import Paginator from 'react-hooks-paginator';
import { useTranslation } from 'react-i18next';
import { Editor } from '@tinymce/tinymce-react';
import TinyMce from '../../config/TinyMce';
import Common from '../../constant/Common';
import { scrollTop } from '../../helpers/Utils';
import { deleteWebService } from '../../helpers/ServerUtils';
import TemplateContext from '../../context/TemplateContext';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import 'react-toastify/dist/ReactToastify.min.css';

const DEFAULT_PREVIEW = 'actual';

const EmailTemplateList = () => {
    const { t, i18n } = useTranslation();
    const styleCursor = {
        cursor: 'pointer'
    };
    
    // Set component state variables
    const [offset, setOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [currentData, setCurrentData] = useState([]);
    const [show, setShow] = useState(false);
    const [preview, setPreview] = useState(DEFAULT_PREVIEW);

    // TemplateContext use
    const { state, dispatch } = useContext(TemplateContext);
        
    // Set title based on length
    const title = typeof state.emailTemplates !== "undefined" && state.emailTemplates.length 
        ? `${state.emailTemplates.length} ${t('template.title.label')}` : 'No Template found';

    // Extract list data as per offset and page limit
    useEffect(() => {
        setCurrentData(state.emailTemplates.slice(offset, offset + Common.PAGE_LIMIT));
    }, [offset, state.emailTemplates]);
    
    // Toaster Success Message
    const notify = (message) => {
        toast.success(message);
    };
    
    // Edit Email Template Handler
    const editEmailTemplate = (item, isPreview) => {
        console.log(item);
        
        // Trigger action creator
        dispatch({
            type: 'SET_SELECTED_EMAIL_TEMPLATE',
            payload: item
        });

        setPreview(DEFAULT_PREVIEW);
        console.log('preview = ', preview);
        isPreview ? handleShow() : scrollTop();
    };

    // Delete Email Template Handler
    const deleteEmailTemplate = async (item) => {
        // Invoke API call with appropriate url
        const response = await deleteWebService(`template/${item._id}`);

        // Extract API responses
        const { status,  message } = response.data;
        
        // Handle API response
        if (status === 'success') {
            dispatch({ 
                type: "DELETE_EMAIL_TEMPLATE", 
                payload: item 
            });
        }

        // Render success message
        notify(message);
    };  

    // Hide Modal
    const handleClose = () => setShow(false);

    // Show Modal
    const handleShow = () => setShow(true);

    const clickHandler = (cb, item) => {
        confirmAlert({
            title: 'Delete Template',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => cb(item)
                },
                {
                    label: 'No',
                    onClick: () => console.log('Click No')
                }
            ]
        });
    };

    const handleEditorChange = (content, editor) => {
        console.log('Content was updated:', content);
    };

    const renderTinyMceEditor = () => {
        return <Editor disabled={true}
            apiKey={TinyMce.API_KEY}
            initialValue=""
            init={TinyMce.initOptions}
            onEditorChange={handleEditorChange}
            value={state.selectedEmailTemplate.body}
        />;
    };

    const togglePreview = (type) => {
        setPreview(type);
    };
            
    return (
        <div>
            <h3 className="mb-4">{title}</h3>            

            {state.emailTemplates.length > 0 && <ul className="list-group">
                <li key="header" className="list-group-item d-flex justify-content-between align-items-center">
                    <span className="col-sm-10 pl-3 font-weight-bold"> {t('template.name.label')} </span>
                    <span className="col-sm-2"></span>
                </li>
                { 
                    currentData.map((item) => (
                        <li key={item._id} className="list-group-item d-flex justify-content-between align-items-center">
                            <span className="col-sm-10" >
                                <div className="text-primary" style={styleCursor}
                                    onClick={ () => editEmailTemplate(item, true) }>{item.name}</div>
                                <div className="small text-secondary">
                                    (Created at: <Moment date={item.createdAt} format="DD-MMM-YYYY" />)
                                </div>
                                {/* { ReactHtmlParser (item.body) } */}
                            </span>

                            <span className="col-sm-2 text-right">                                
                                <span className="ml-4" data-tip={t('template.preview.label')} 
                                    onClick={ () => editEmailTemplate(item, true) }
                                    style={styleCursor}>
                                    <i className="fas fa-eye text-primary"></i>
                                </span>

                                <span className="ml-4" data-tip={t('template.edit.label')}
                                    onClick={ () => editEmailTemplate(item, false) }
                                    style={styleCursor}>                                    
                                    <i className="fas fa-edit text-primary"></i>
                                </span>     
                                                           
                                <span className="ml-4" data-tip={t('template.delete.label')}
                                    onClick={ () => clickHandler(deleteEmailTemplate, item) }
                                    style={styleCursor}>
                                    <i className="fas fa-trash text-danger"></i>
                                </span>

                                <ReactTooltip />
                            </span>
                        </li>
                    ))
                }                
            </ul>}
            
            <Paginator
                totalRecords={state.emailTemplates.length || 0}
                pageLimit={Common.PAGE_LIMIT}
                pageNeighbours={1}
                setOffset={setOffset}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
            />           
            
            <ToastContainer position="bottom-right" autoClose={2000} />

            <Modal size="lg" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        {t('template.preview.label')} ({state.selectedEmailTemplate.name})
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="text-right mb-4">
                        {preview === 'editor' && <span className="text-primary" style={styleCursor}
                            onClick={() => togglePreview('actual')}> 
                        </span>}
                        {preview === 'actual' && <span className="text-primary" style={styleCursor}
                            onClick={() => togglePreview('editor')}> Switch to Editor Preview
                        </span>}
                    </div>
                    {/* <div className="mb-3">
                        <span className="font-weight-bold">Subject:</span>
                        <span className="ml-2">{ state.selectedEmailTemplate.subject }</span>
                    </div> */}

                    {/* <div className="font-weight-bold">Body:</div> */}
                    <div>                        
                        { preview == 'actual' && ReactHtmlParser (state.selectedEmailTemplate.body) }
                        { preview == 'editor' && renderTinyMceEditor() }
                    </div>
                </Modal.Body>
                
                {/* <Modal.Footer>
                    <button type="button" className="btn btn-secondary" onClick={handleClose}>
                        Close
                    </button>
                    <button type="button" className="btn btn-primary" onClick={handleClose}>
                        Save
                    </button>
                </Modal.Footer> */}
            </Modal>
        </div>
    );
};

export default EmailTemplateList;
import React, { useContext, useReducer, useEffect } from 'react';

import { useAPI } from '../../helpers/Utils';
import TodoContext from '../../context/TodoContext';
import TodosReducer from '../../reducers/TodoReducer';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

const Todo = () => {
	const initialState = useContext(TodoContext);
	const [state, dispatch] = useReducer(TodosReducer, initialState);	

	const todos = useAPI("todo");

	useEffect(
		() => {
			dispatch({
				type: "GET_TODOS",
				payload: todos
			});
		},
		[todos]
	);

	return (
			<TodoContext.Provider value={{ state, dispatch }}>
			<TodoForm />
			<TodoList />
			</TodoContext.Provider>
	);
};

export default Todo;
import React, { useState, useContext, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import TodoContext from '../../context/TodoContext';
import { postWebService, putWebService, uploadWebService } from '../../helpers/ServerUtils';

const TodoForm = () => {
    const [todo, setTodo] = useState('');
    const [file, setFile] = useState('');
    const [error, setError] = useState('');

    const {state: { selectedTodo = {} }, dispatch} = useContext(TodoContext);

    useEffect(() => {
        setTodo(selectedTodo.name || '');
    }, [selectedTodo._id]);

    const notify = (message) => {
        toast.success(message);
    };

    const cb = function ({ data, status,  message }, type) {
        console.log(status);

        if (status === 'success') {
            dispatch({ type: type, payload: data });
            notify(message);
            setFile('');
        } else {
            setError(() => parseMessage(message));
            console.log(error);
		}         
    };

    const saveTodo = async () => {
        const response = await postWebService('todo', { name: todo });

        cb(response.data, "ADD_TODO");    
    };

    const updateTodo = async () => {
        const response = await putWebService(`todo/${selectedTodo._id}`, { name: todo });
        
        cb(response.data, "UPDATE_TODO"); 
    };  

    const handleSubmit = event => {
        event.preventDefault();
        
        selectedTodo.name ? updateTodo() : saveTodo();
        setTodo("");
    };

    const buildFormData = () => {
        const formData = new FormData();
        formData.append('filename', file);

        return formData;
    };

    const uploadCsvFile = async () => {
        const formData = buildFormData();

        const response = await uploadWebService('todo/bulk-upload', formData);
        console.log(response.data);

        cb(response.data, "BULK_ADD_TODOS");          
    };

    const handleBulkSubmit = event => {
        event.preventDefault();
        uploadCsvFile();
    };

    const handleChange = (event) => {
        setFile(event.target.files[0]);        
    };
    
    const createFakeData = async () => {
        const response = await postWebService('fake-todo', { });
        console.log(response.data);

        cb(response.data, "BULK_ADD_TODOS");
    };

    return (
        <>        
            <div className="border p-2">
                <h3 className="mb-4">Upload Bulk Todos</h3>

                <form onSubmit={handleBulkSubmit} enctype="multipart/form-data">
                    <div className="form-row">                    
                        <div className="col">
                            <input type="file" id="myFile" name="myImage" onChange={handleChange} />
                        </div>
                        <div className="col">
                            <button type="submit" className="btn btn-primary" disabled={!file}>Submit</button>
                        </div>
                    </div>
                </form>
            </div>

            <div className="mt-4">
                <form onSubmit={handleSubmit}>
                    <div className="form-row mb-5">
                        <div className="col">
                            <input type="text" name="name" className="form-control" placeholder="Enter Todo Text"
                                onChange={(event) => setTodo(event.target.value)}
                                value={todo} />
                        </div>
                        <div className="col">
                            {error && <div className="text-danger mb-2">{error}</div>}
                            <button type="submit" className="btn btn-primary" disabled={!todo}>
                                {selectedTodo.name ? 'Update' : 'Save'}
                            </button>
                            <span className="ml-4">OR</span>
                            <button type="button" className="btn btn-success ml-4" onClick={createFakeData}>
                                Create Fake Data (Approx 500 records)
                            </button>
                        </div>                
                    </div>
                </form>
            </div>                       

            <ToastContainer position="bottom-right" autoClose={10000} />
        </>
    );
};

export default TodoForm;


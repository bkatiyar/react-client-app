import React, { useState, useEffect, useContext, useRef } from 'react';
import Moment from 'react-moment';
import { ToastContainer, toast } from 'react-toastify';
import ReactTooltip from "react-tooltip";
import Paginator from 'react-hooks-paginator';
import { CSVLink } from "react-csv";
import { JsonToExcel } from 'react-json-excel';

import TodoContext from '../../context/TodoContext';
import { patchWebService, deleteWebService } from '../../helpers/ServerUtils';
import { CsvHeaderColumns } from '../../helpers/CsvHeaderColumns';

import 'react-toastify/dist/ReactToastify.min.css';

const TodoList = () => {
    // Pagination related constant declaration
    const pageLimit = 10;
    const styleFilter = {
        width: '220px',        
        padding: '6px',
        marginTop: '8px',
        marginRight: '16px',
        border: 'none',
        fontSize: '17px'
    };
    const styleCursor = {
        cursor: 'pointer'
    };

    // Set component state variables
    const [offset, setOffset] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [currentData, setCurrentData] = useState([]);
    const [selectedIds, setSelectedIds] = useState([]);
    const [checked, setChecked] = useState(false);
    const [filter, setFilter] = useState('');
    const [searchTerm, setSearchTerm] = useState('');
    const [sortKey, setSortKey] = useState('');
    const [sortType, setSortType] = useState('asc');
    const [sortBy, setSortBy] = useState('');

    // Set component references
    const inputElementCheckbox = useRef(null);

    // TodoContext use
    const { state, dispatch } = useContext(TodoContext);
        
    // Set Todos title based on length
    const title = typeof state.todos !== "undefined" && state.todos.length ? `${state.todos.length} Todos` : 'No Todo found';

    // Extract list data as per offset and page limit
    useEffect(() => {
        setCurrentData(state.todos.slice(offset, offset + pageLimit));
    }, [offset, state.todos, selectedIds, checked]);
    
    // Toaster Success Message
    const notify = (message) => {
        toast.success(message);
    };

    // Todo Change Status Handler
    const changeStatus = async (todo) => {
        // Invoke API call with appropriate url & payload
        const response = await patchWebService(`todo/${todo._id}`, { status: todo.status === 1 ? 2 : 1 });
        
        // Extract API response
        const { data, status,  message } = response.data;
        
        // Handle API response
        if (status === 'success') {
            dispatch({ type: "CHANGE_STATUS_TODO", payload: data });
        }

        // Render success message
        notify(message);
    };

    // Edit Todo Handler
    const editTodo = (todo) => {
        // Trigger action creator
        dispatch({
            type: 'SET_SELECTED_TODO',
            payload: todo
        });
    };

    // Delete Todo Handler
    const deleteTodo = async (todo) => {
        // Invoke API call with appropriate url
        const response = await deleteWebService(`todo/${todo._id}`);

        // Extract API responses
        const { status,  message } = response.data;
        
        // Handle API response
        if (status === 'success') {
            dispatch({ 
                type: "DELETE_TODO", 
                payload: todo 
            });
        }

        // Render success message
        notify(message);
    };
    
    const bulkSuceessErrorCallback = ({ data, status,  message }) => {
        // Check within API response
        if (status === 'success') {
            dispatch({
				type: "GET_TODOS",
				payload: data
            });
            setSelectedIds([]);
            inputElementCheckbox.current.checked = false;
            setChecked(false);
        }

        // Render success message
        notify(message);
    };

    const bulkUpdateStatus = async (value) => {
        // Build payload
        const payload = { status: value };

        // Convert selected todos array to string
        const ids = selectedIds.join(',');

        // Invoke update status API call
        const response = await patchWebService(`todo/bulk-update/${ids}`, payload);
        
        // Success and error callback
        bulkSuceessErrorCallback(response.data);
    };
    
    const bulkDelete = async () => {
        // Convert selected todos array to comma seperated string
        const ids = selectedIds.join(',');

        // Invoke API call with appropriate url
        const response = await deleteWebService(`todo/bulk-update/${ids}`);        
        
        // Success & Error callback function
        bulkSuceessErrorCallback(response.data);
    };
    
    const checkboxChangeHandler = (_id) => {
        // Created shallow copy of array
        const shallowCopy = [...selectedIds];

        // Find index of the element if it exists otherwise will return -1
        const findIndex = shallowCopy.indexOf(_id);

        // Add remove item from array based on find index
        findIndex > -1 ? shallowCopy.splice(findIndex, 1) : shallowCopy.push(_id);

        // Made Check/Un-check all checked based on condition
        inputElementCheckbox.current.checked = shallowCopy.length === pageLimit;

        // Set new shallow copy to component state
        setSelectedIds(shallowCopy);
    };

    // Select all checkbox click handler
    const checkAllHandler = (event) => {
        // Set seleted todos to empty array
        setSelectedIds([]);

        // If checkbox is checked then extract all the todos ids, otherwise set component state variable to empty array
        if (event.target.checked) {
            let ids = currentData.map(item => item._id);
            setSelectedIds(ids);
        } else {
            setSelectedIds([]);
        }
    };

    // Status filter dropdown change handler
    const changeDropdownHandler = (event) => {
        // Set component state filter
        setFilter(event.target.value);

        // Trigger dispatch event
        dispatch({ 
            type: "FILTER_TODOS", 
            payload: event.target.value 
        });        
    };

    // Search text input change handler
    const handleChange = () => {
        // Set component state seach term
        setSearchTerm(event.target.value);

        // Trigger dispatch event
        dispatch({ 
            type: "SEARCH_TODOS", 
            payload: event.target.value 
        });
    };

    // Table column header sort by click handler
    const onSort = (event, keyName) => {
        // Set component state sort key
        setSortKey(keyName);
        
        // Set component state sort type
        if (!sortType) {
            setSortType('asc');
        } else {
            sortType === 'asc' ? setSortType('desc') : setSortType('asc');
        }

        // Trigger dispatch event
        dispatch({
            type: "SORT_TODOS",
            payload: {
                sortKey: keyName,
                sortType: sortType
            }
        });
    };

    // Sort by filter dropdown handler
    const changeSortBy = (event) => {
        // Set component state sort by
        setSortBy(event.target.value);

        // Set component state sort key and sort by
        if (event.target.value) {
            const soryByArray = event.target.value.split(':');
            setSortKey(soryByArray[0]);
            setSortType(soryByArray[1]);
            
            // Trigger dispatch event
            dispatch({
                type: "SORT_TODOS",
                payload: {
                    sortKey: soryByArray[0],
                    sortType: soryByArray[1]
                }
            });
        } else {
            setSortKey('name');
            setSortType('asc');
            dispatch({
                type: "RESET_SORT_TODOS",
                payload: [...state.actualTodos]
            });
        }        
    };    
            
    return (
        <div>
            <h3 className="mb-4">{title}</h3>
            
            <div className="row mb-3">
                <div className="col-sm-4">
                    <input type="text" placeholder="Search..." value={searchTerm}
                        onChange={handleChange} style={styleFilter} />
                </div>
                <div className="col-sm-4 text-center">
                    <select id="todoStatus" className="ml-2" value={filter} 
                        onChange={changeDropdownHandler} style={styleFilter}>
                        <option value="">All Todos</option>
                        <option value="1">Pending</option>
                        <option value="2">Completed</option> 
                    </select>
                </div>
                {state.todos.length > 0 && <div className="text-right col-sm-4">
                    <select id="sortBy" className="ml-2" value={sortBy} 
                        onChange={changeSortBy} style={styleFilter}>
                        <option value="">Recent Todos</option>
                        <option value="name:asc">Name: Ascending</option>
                        <option value="name:desc">Name: Descending</option>
                        <option value="createdAt:asc">Created By: Ascending</option>
                        <option value="createdAt:desc">Created By: Descending</option> 
                    </select>
                </div>}
            </div>

            {state.todos.length > 0 && <div className="row mb-3">
                <div className="col-sm-2 pl-3">
                    <input type="checkbox" ref={inputElementCheckbox} className="mr-2"
                        onChange={checkAllHandler} /> Select All
                </div>
                <div className="col-sm-4"></div>
                <div className="text-right col-sm-6">
                    {filter === "1" && <button type="button" className="btn btn-success btn-sm"
                        data-tip="Mark as Complete in bulk"
                        { ...( !selectedIds.length && { disabled: true } ) }
                        onClick={() => bulkUpdateStatus(2)}>Bulk Mark as Complete</button>}

                    {filter === "2" && <button type="button" className="btn btn-secondary btn-sm ml-2"
                        data-tip="Mark as Pending in Bulk" 
                        { ...( !selectedIds.length && { disabled: true } ) }
                        onClick={() => bulkUpdateStatus(1)}>Bulk Mark as Pending</button>}

                    <button type="button" className="btn btn-danger btn-sm ml-2"
                        data-tip="Delete in Bulk" 
                        { ...( !selectedIds.length && { disabled: true } ) }
                        onClick={bulkDelete}>Bulk Delete</button>

                    <CSVLink
                        headers={CsvHeaderColumns.todos}
                        data={state.todos || []}
                        filename={"my-todos.csv"}
                        className="btn btn-primary btn-sm ml-2"
                        target="_blank"
                        data-tip="Download Todos in CSV Format">
                        <i className="fas fa-download"></i> Download
                    </CSVLink>

                    {/* <JsonToExcel
                        data={state.todos || []}
                        className={"btn btn-primary btn-sm ml-2"}
                        filename={"my-todos.xls"}
                        fields={CsvHeaderColumns.todos}
                    /> */}
                </div>        
            </div>}

            {state.todos.length > 0 && <ul className="list-group">
                <li key="header" className="list-group-item d-flex justify-content-between align-items-center">
                    <span></span>
                    <span className="col-sm-6 pl-4 font-weight-bold" 
                        onClick={(event) => onSort(event, 'name')}> 
                        Text {(sortKey === 'name' && sortType === 'desc') && <i className="fas fa-sort-up"></i>}
                        {(sortKey === 'name' && sortType === 'asc') && <i className="fas fa-sort-down"></i>}
                    </span>
                    <span className="col-sm-2 pl-4 font-weight-bold" 
                        onClick={(event) => onSort(event, 'createdAt')}> 
                        Date {(sortKey === 'createdAt' && sortType === 'desc') && <i className="fas fa-sort-up"></i>}
                        {(sortKey === 'createdAt' && sortType === 'asc') && <i className="fas fa-sort-down"></i>}
                    </span>
                    <span className="col-sm-4"></span>
                </li>
                { 
                    currentData.map((todo) => (
                        <li key={todo._id} className="list-group-item d-flex justify-content-between align-items-center">
                            <span>
                                <input id={todo._id} type="checkbox"
                                    {...(selectedIds.indexOf(todo._id) > -1 ? {checked: true} : {}) }
                                    onClick={() => checkboxChangeHandler(todo._id)} />
                            </span>

                            <span className="col-sm-6" >
                                {todo.status === 2 && <del>{todo.name}</del>}
                                {todo.status === 1 && todo.name}                                                               
                            </span>

                            <span className="col-sm-2 small">
                                <Moment date={todo.createdAt} format="DD-MMM-YYYY" />
                            </span>

                            <span className="col-sm-4 text-right">
                                <span className="col-sm-2" >
                                    {todo.status === 1 && <span className="badge badge-success ml-2 p-2"
                                        onClick={ () => changeStatus(todo) } style={styleCursor}
                                        data-tip="Mark as Complete"> Mark as Complete</span>}

                                    {todo.status === 2 && <span className="badge badge-secondary ml-2 p-2"                                     
                                        onClick={ () => changeStatus(todo) } style={styleCursor}
                                        data-tip="Mark as Pending">Mark as Pending</span>} 
                                </span>

                                <span className="col-sm-1" data-tip="Edit"
                                    onClick={ () => editTodo(todo) }>                                    
                                    <i className="fas fa-edit text-primary"></i>
                                </span>     
                                                           
                                <span className="col-sm-1" data-tip="Delete"
                                    onClick={ () => deleteTodo(todo) }>
                                    <i className="fas fa-trash text-danger"></i>
                                </span>
                                <ReactTooltip />
                            </span>
                        </li>
                    ))
                }                
            </ul>}
            
            <Paginator
                totalRecords={state.todos.length || 0}
                pageLimit={pageLimit}
                pageNeighbours={1}
                setOffset={setOffset}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
            />           
            
            <ToastContainer position="bottom-right" autoClose={2000} />
        </div>
    );
};

export default TodoList;
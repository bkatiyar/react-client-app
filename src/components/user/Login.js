import React, { useState, useEffect, useContext }  from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import Cookie from "js-cookie";

import UserContext from '../../context/UserContext';
import { parseMessage } from '../../helpers/Utils';
import { postWebService } from '../../helpers/ServerUtils';

const initialFormState = {
	email: "",
	password: ""
};

const Login = () => {
	const [form, setForm] = useState(initialFormState);
	const [error, setError] = useState('');
	const { dispatch } = useContext(UserContext);
	const history = useHistory();
	
	useEffect(() => {

	}, [error]);

	const notify = (message) => {
        toast.success(message);
	};
	
	const login = async () => {
		const payload = Object.assign({}, form);
		const headers = null;		
		const response = await postWebService('users/login', payload, headers);
		const { data, status, message } = response.data;

		if (status === 'success') {
			dispatch({ type: 'SET_CURRENT_USER', payload: data});
			Cookie.set("token", data.token);
			notify(message);			
			history.replace("/");
		} else {
			setError(() => parseMessage(message));
		}
	};	

	const submitHandler = (event) => {
		event.preventDefault();
		login();
	};

	const handleChange = (event) => {
		setForm({
			...form,
			[event.target.name]: event.target.value
		});
	};

	return (
		<div>
			<h3 className="mb-4">Login Section</h3>
			<form className="was-validated" onSubmit={submitHandler}>
				<div className="form-group">
					<label>Email:</label>
					<input type="email" className="form-control" id="email" 
						placeholder="Enter email" name="email" required
						onChange={handleChange} value={form.email} />

					<div className="valid-feedback">Valid.</div>
					<div className="invalid-feedback">Please fill out this field.</div>
				</div>

				<div className="form-group">
					<label>Password:</label>
					<input type="password" className="form-control" id="password" 
						placeholder="Enter password" name="password" required
						onChange={handleChange} value={form.password}  />
					<div className="valid-feedback">Valid.</div>
					<div className="invalid-feedback">Please fill out this field.</div>
				</div>

				{error && <div className="text-danger mb-2">{error}</div>}
				<ToastContainer position="bottom-right" autoClose={10000} />

				<div className="row">
					<div className="col-sm-1" >
						<button type="submit" className="btn btn-primary">Submit</button>
					</div>
					<div className="col-sm-4" >
						<NavLink to="/register" className="nav-link ml-5">Don't have an account? Sign Up</NavLink>
					</div>
				</div>
			</form>
		</div>	
	);
};

export default Login;
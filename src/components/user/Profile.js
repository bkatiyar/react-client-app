import React, { useState } from 'react';
import { uploadWebService } from '../../helpers/ServerUtils';

const Profile = () => {
    const [file, setFile] = useState('');
    const [profilePic, setProfilePic] = useState('');
    const [profileThumb, setProfileThumb] = useState('');
    const [isUploaded, setIsUploaded] = useState(false);

    const buildFormData = () => {
        const formData = new FormData();
        formData.append('filename', file);

        return formData;
    };

    const updateProfilePic = async () => {
        const formData = buildFormData();
        //profile-thumb
        const response = await uploadWebService('users/profile', formData);
        const { status, data } = response.data || null;
        const { pic, thumb } = data || null;

        console.log(data);

        if (status === 'success') {
            setProfilePic(pic);
            setProfileThumb(thumb);
            setIsUploaded(true);
        }        		
    };

    const submitHandler = (event) => {
        event.preventDefault();
        updateProfilePic();        
    };
    const handleChange = (event) => {
        setFile(event.target.files[0]);        
	};
    
	return (
		<div>
			<h3 className="mb-4">Profile Section</h3>
			<form enctype="multipart/form-data" onSubmit={submitHandler}>
				<div className="form-group">
					<label>Profile Pic:</label>
                    <input type="file" id="myFile" name="myImage" onChange={handleChange} />
				</div>
                
                <div className="row">
					<div className="col-sm-1" >
						<button type="submit" className="btn btn-primary">Submit</button>
					</div>
				</div>
                
                {isUploaded && <div className="row mt-3">
                    <h3>Uploaded Response</h3>
                    <div className="row mt-2">
                        <div className="col-sm-3"> <img src={profileThumb} /></div>
                        <div className="col-sm-9"> <img src={profilePic} /></div>
                    </div>
                </div>}               
            </form>
        </div>
	);
};

export default Profile;
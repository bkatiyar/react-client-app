import React, { useContext } from 'react';
import UserContext from '../../context/UserContext';

const UserProfile = () => {
    const { state } = useContext(UserContext);
    const { currentUser: { username, email } } = state || null;

    return (
        <div>
            <div className="row">User Name: {username}</div>
            <div className="row">User Email: {email}</div>
        </div>
    );
};

export default UserProfile;
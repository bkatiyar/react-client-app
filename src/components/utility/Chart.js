import React, { useContext } from 'react';
import ReactHighcharts from 'react-highcharts';

const styleFilter = {
	width: '220px',        
	padding: '6px',
	marginTop: '8px',
	border: 'none'
};

const Chart = (props) => {
    const { 
		changeSequence,		
		options, 
		selectedSequenceId,
		sequences,
		title,
		visible,
		chartContainer 
	} = props;
	
	const afterRender = (chart) => {
        //console.log(chart);
	};

	const renderChart = () => {
		return (
			<div className="card">
				<div className="card-header">
					<span className="font-weight-bold">{title}</span>
					{visible && <span className="text-right col-sm-5" style={{display: 'inline-table'}}>
						<select className="ml-2" value={selectedSequenceId} 
							onChange={changeSequence} style={styleFilter}>
							{
								sequences.map((sequence) => (
									<option key={sequence._id} value={sequence._id}>{sequence.name}</option>
								))
							}
						</select>
					</span>}
				</div>
				<div className="card-body text-center text-white" style={{ 'padding': '1.25rem 0' }}>
					<ReactHighcharts config = {options} callback = {afterRender}></ReactHighcharts>
				</div>
			</div>
		);
	};

	if (chartContainer === 'full-width') {
		return <div className="card-deck mt-3">{renderChart()}</div>;
	} else {
		return renderChart();
	}    
};

export default Chart;
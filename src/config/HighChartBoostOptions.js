export default {
    chart: {
        type: 'scatter',
        zoomType: 'xy',
        height: '100%'
    },
    boost: {
        useGPUTranslations: true,
        usePreAllocated: true
    },
    xAxis: {
        title: {
            text: 'Goal'
        },
        min: 0,
        max: 3,
        tickInterval: 1,
        labels: {
            step: 1
        },
        gridLineWidth: 1
    },
    yAxis: {
        min: 0,
        max: 3,
        minPadding: 0,
        maxPadding: 0,
        title: {
            text: 'Competency'
        },
        tickInterval: 1,
        labels: {
            step: 1
        }
    },
    title: {
        text: 'Performance Measument'
    },
    legend: {
        enabled: false
    },
    series: []
    // series: [{        
    //     color: 'rgb(152, 0, 67)',
    //     fillOpacity: 0.1,
    //     data: [],
    //     marker: {
    //         radius: 3
    //     },
    //     tooltip: {
    //         followPointer: false,
    //         pointFormat: '[{point.x:.1f}, {point.y:.1f}]'
    //     }
    // }]
};
export default {    
    chart: {
        type: 'column',
        animation: true,
        styledMode: true,
        height: '250px'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        x: 0,
        y: -15,
        floating: true
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Email'
        },
        gridLineColor: 'transparent'       
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0,
            //grouping: false, // for stacked column graph
            borderRadius: 4
        },
        series: {
            animation: {
                duration: 2000      
            },
            maxPointWidth: 14
        }
    },
    credits: {
        enabled: false
    },
    series: []    
};
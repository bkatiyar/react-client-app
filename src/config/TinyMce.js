export default {
    API_KEY: "u5d2f0d7p6kfvz264ff7w0qcg8k6h9yk5ntmqbhi90pzje6w",
    initOptions: {                                
        height: 500,
        menubar: false,
        mode : "none",
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar:
            'undo redo | formatselect | bold italic backcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help | btnReferenceList',
        setup: function (editor) {
            editor.ui.registry.addSplitButton('btnReferenceList', {
                text: 'Reference List',
                onAction: function () {
                    //editor.insertContent('<p>You clicked the main button</p>');
                },
                onItemAction: function (api, value) {
                    console.log(value);
                    editor.insertContent(value);
                },
                fetch: function (callback) {
                    var items = [
                        {
                            type: 'choiceitem',
                            text: 'Username',
                            value: '{username}'
                        },
                        {
                            type: 'choiceitem',
                            text: 'Firstname',
                            value: '{firstname}'
                        },
                        {
                            type: 'choiceitem',
                            text: 'Lastname',
                            value: '{lastname}'
                        }
                    ];
                    callback(items);                
                }
            });
        }
    }
};
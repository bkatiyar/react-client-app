const Todo = {
    LIST: 'todo_list',
    SAVE: 'todo_save',
    UPDATE: 'todo_update',
    DELETE: 'todo_delete',
    CHANGE_STATUS: 'todo_change_status'
};

export default Todo;
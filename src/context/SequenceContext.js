import React from 'react';

const EmailSequenceContext = React.createContext({
    emailSequences: [],
    selectedEmailSequence: {}
});

export default EmailSequenceContext;

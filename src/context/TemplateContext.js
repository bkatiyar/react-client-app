import React from 'react';

const EmailTemplateContext = React.createContext({
    emailTemplates: [],
    selectedEmailTemplate: {}
});

export default EmailTemplateContext;

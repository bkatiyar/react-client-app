import React from 'react';

const TodoContext = React.createContext({
    todos: [],
    selectedTodo: {}
});

export default TodoContext;

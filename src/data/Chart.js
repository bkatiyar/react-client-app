// Build categories for template
export const categoriesTemplate = [
    'Propsal Follow Up 1',
    'Propsal Follow Up 2',
    'Propsal Follow Up 3',
    // 'Product Follow Up 1',
    // 'Product Follow Up 2',
    // 'Implementation Follow Up 1',
    'Go Live Follow Up 1'
];

// Build series for template
export const seriesTemplate = [{
    name: 'Sent',
    color: '#12ce66',
    data: [100, 200, 150, 180]
}, {
    name: 'Opened',
    color: '#8669ff',
    data: [70, 110, 100, 90]
}, {
    name: 'Replied',
    color: '#f8c763',
    data: [30, 50, 30, 20]
}, {
    name: 'Bounced',
    color: '#aeb7bc',
    data: [10, 30, 20, 12]
}];

// Build categories for sequence
export const categoriesSequence = [
    'Propsal Sequence',
    'Demo Request Sequence',
    'Payment Sequence',
    'Implementation Sequence',
    'Go Live Sequence'
];

// Build series for sequence
export const seriesSequence = [{
    name: 'Sent',
    color: '#12ce66',
    data: [35, 30, 25, 32]
}, {
    name: 'Opened',
    color: '#8669ff',
    data: [20, 18, 15, 17]
}, {
    name: 'Replied',
    color: '#f8c763',
    data: [4, 3, 5, 6]
}, {
    name: 'Bounced',
    color: '#aeb7bc',
    data: [2, 3, 1, 4]
}];

// Build categories for sequence daywise
export const categoriesDaywise = [
    'Mon',
    'Tues',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun'
];

// Build series for sequence daywise
export const seriesDaywise = [{
    name: 'Sent',
    color: '#12ce66',
    data: [5, 5, 7, 4, 6, 8, 3]
}, {
    name: 'Opened',
    color: '#8669ff',
    data: [3, 4, 5, 2, 4, 6, 1]
}, {
    name: 'Replied',
    color: '#f8c763',
    data: [1, 2, 2, 1, 1, 2, 1]
}, {
    name: 'Bounced',
    color: '#aeb7bc',
    data: [1, 3, 2, 1, 0, 2, 2]
}];

// Build categories for followup wise
export const categoriesFollowUpwise = [
    'Propsal Follow Up 1',
    'Propsal Follow Up 2',
    'Propsal Follow Up 3'
];

// Build series for followup wise
export const seriesFollowUpwise = [{
    name: 'Sent',
    color: '#12ce66',
    data: [5, 5, 7]
}, {
    name: 'Opened',
    color: '#8669ff',
    data: [3, 4, 5]
}, {
    name: 'Replied',
    color: '#f8c763',
    data: [1, 2, 2]
}, {
    name: 'Bounced',
    color: '#aeb7bc',
    data: [1, 3, 2]
}];

// Build categories for sequence hourwise
export const categoriesHourwise = [
    '09:00 AM - 12:00 PM',
    '12:00 PM - 03:00 PM',
    '03:00 PM - 06:00 PM',
    '06:00 PM - 09:00 PM',
    '09:00 PM - 12:00 AM',
    '12:00 AM - 03:00 AM',
    '03:00 AM - 06:00 AM',
    '06:00 AM - 09:00 AM'
];

// Build series for sequence hourwise
export const seriesHourwise = [{
    name: 'Sent',
    color: '#12ce66',
    data: [4, 1, 0, 1, 1, 0, 1, 3]
}, {
    name: 'Opened',
    color: '#8669ff',
    data: [1, 1, 0, 1, 1, 0, 0, 1]
}, {
    name: 'Replied',
    color: '#f8c763',
    data: [1, 0, 0, 1, 0, 0, 0, 1]
}, {
    name: 'Bounced',
    color: '#aeb7bc',
    data: [1, 0, 0, 0, 0, 0, 1, 1]
}];

// Build categories for sequence day to get reply
export const categoriesSequenceDayToGetReply = [
    'Propsal Sequence',
    'Demo Request Sequence',
    'Payment Sequence',
    'Implementation Sequence',
    'Go Live Sequence'
];

// Build series for sequence day to get reply
export const seriesSequenceDayToGetReply = [{
    name: 'Replied',
    color: '#f8c763',
    data: [5, 3, 1, 4]
}];

// Build categories for sequence wise open rate
export const categoriesSequencewiseOpenRate = [
    'Propsal Sequence',
    'Demo Request Sequence',
    'Payment Sequence',
    'Implementation Sequence',
    'Go Live Sequence'
];

// Build series for sequence wise open rate
export const seriesSequencewiseOpenRate = [{
    name: 'Replied',
    color: '#8669ff',
    data: [37, 98, 58, 76]
}];
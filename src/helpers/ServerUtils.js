import axios from 'axios';
import Cookie from "js-cookie";
import Config from '../config/Config';

const getApiUrl = (url) => {
    return `${Config.apiUrl}${url}`;
};

export const postWebService = async (url, payload, headers = { token : Cookie.get("token") || null }) => {
    return await axios({
        url: getApiUrl(url),
        data: payload,
        method: 'post',
        headers: headers
    });
};

export const putWebService = async (url, payload, headers = { token : Cookie.get("token") || null }) => {
    return await axios({
        url: getApiUrl(url),
        data: payload,
        method: 'put',
        headers: headers
    });
};

export const patchWebService = async (url, payload, headers = { token : Cookie.get("token") || null }) => {
    return await axios({
        url: getApiUrl(url),
        data: payload,
        method: 'patch',
        headers: headers
    });
};

export const deleteWebService = async (url, headers = { token : Cookie.get("token") || null }) => {
    return await axios({
        url: getApiUrl(url),
        method: 'delete',
        headers: headers
    });
};

export const getWebService = async (url, headers = { token : Cookie.get("token") || null }) => {
    return await axios({
        url: getApiUrl(url),
        method: 'get',
        headers: headers
    });
};

export const uploadWebService = async (url, payload, headers = { token : Cookie.get("token"), Accept: 'application/json', 'Content-Type': 'multipart/form-data'}) => {
    return await axios({
        url: getApiUrl(url),
        data: payload,
        method: 'post',
        headers: headers
    });
}; 
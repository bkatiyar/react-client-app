import { useState, useEffect } from 'react';
import { getWebService } from '../helpers/ServerUtils';

export const useAPI = (endpoint) => {
	const [data, setData] = useState([]);
	
	useEffect(() => {
		fetchData();
	}, []);

	const fetchData = async () => {
		const response = await getWebService(endpoint);
	
		setData(response.data.data);
	};

	return data;
};

export const parseMessage = (message) => {
    if (typeof message === "string") {
        return message;
    } else if (Array.isArray(message) && message.length) {
        let array = [];
        const output = message.map(item => [...array, item.msg] );
        return output.join(', ');
    }
};

export const compareValues = (key, order = 'asc') => {
	return function innerSort(a, b) {
		if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
			// property doesn't exist on either object
			return 0;
		}
  
		const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key];
		const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];
  
		let comparison = 0;
		if (varA > varB) {
			comparison = 1;
		} else if (varA < varB) {
			comparison = -1;
		}

		return (
			(order === 'desc') ? (comparison * -1) : comparison
		);
	};
};

export const scrollTop = () => {
	window.scrollTo({
		top: 0,
		behavior: "smooth"
	});
};
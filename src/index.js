import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './css/style.css';
import AppRoutes from './routes/AppRoutes';
import * as serviceWorker from './serviceWorker';
import './i18n';

ReactDOM.render(
	  <React.StrictMode>
      <AppRoutes />
    </React.StrictMode>,
  	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

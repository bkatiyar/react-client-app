export default function reducer(state, action) {
    switch (action.type) {
		case "GET_EMAIL_SEQUENCES":
			return {
				...state,
				emailSequences: action.payload,
				actualEmailSequences: action.payload
			};

		case "ADD_EMAIL_SEQUENCE":
			const addedEmailSequences = [action.payload, ...state.emailSequences];

			return {
				...state,
				emailSequences: addedEmailSequences,
				actualEmailSequences: addedEmailSequences
			};	

		case "SET_SELECTED_EMAIL_SEQUENCE":
			return {
				...state,
				selectedEmailSequence: action.payload
			};
			  
		case "UPDATE_EMAIL_SEQUENCE":
			const updatedEmailSequence = { ...action.payload };
			const updatedEmailSequenceIndex = state.emailSequences.findIndex(
				t => {
					console.log(t._id, updatedEmailSequence._id);

					return t._id === updatedEmailSequence._id
				}
			);
			const updatedEmailSequences = [
				...state.emailSequences.slice(0, updatedEmailSequenceIndex),
				updatedEmailSequence,
				...state.emailSequences.slice(updatedEmailSequenceIndex + 1)
			];

			return {
				...state,
				selectedEmailSequence: {},
				emailSequences: updatedEmailSequences,
				actualEmailSequences: updatedEmailSequences
			};

		case "CHANGE_STATUS_EMAIL_SEQUENCE":
			const toggledEmailSequences = state.emailSequences.map(t =>
				t._id === action.payload._id ? action.payload : t
			);
			return {
				...state,
				emailSequences: toggledEmailSequences,
				actualEmailSequences: toggledEmailSequences
			};

		case "DELETE_EMAIL_SEQUENCE":
			const filteredEmailSequences = state.emailSequences.filter(t => t._id !== action.payload._id);
			const isRemovedEmailSequence = state.selectedEmailSequence._id === action.payload._id 
				? {} : state.selectedEmailSequence;
			
			return {
				...state,
				selectedEmailSequence: isRemovedEmailSequence,
				emailSequences: filteredEmailSequences,
				actualEmailSequences: filteredEmailSequences
			};
				
		default:
			return state;
    }
}
  
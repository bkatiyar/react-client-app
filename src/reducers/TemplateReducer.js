//import { compareValues } from '../helpers/Utils'; 

export default function reducer(state, action) {
    switch (action.type) {
		case "GET_EMAIL_TEMPLATES":
			return {
				...state,
				emailTemplates: action.payload,
				actualEmailTemplates: action.payload
			};

		case "ADD_EMAIL_TEMPLATE":
			const addedEmailTemplates = [action.payload, ...state.emailTemplates];

			return {
				...state,
				emailTemplates: addedEmailTemplates,
				actualEmailTemplates: addedEmailTemplates
			};	

		case "SET_SELECTED_EMAIL_TEMPLATE":
			return {
				...state,
				selectedEmailTemplate: action.payload
			};
			  
		case "UPDATE_EMAIL_TEMPLATE":
			const updatedEmailTemplate = { ...action.payload };
			const updatedEmailTemplateIndex = state.emailTemplates.findIndex(
				t => {
					console.log(t._id, updatedEmailTemplate._id);

					return t._id === updatedEmailTemplate._id
				}
			);
			const updatedEmailTemplates = [
				...state.emailTemplates.slice(0, updatedEmailTemplateIndex),
				updatedEmailTemplate,
				...state.emailTemplates.slice(updatedEmailTemplateIndex + 1)
			];

			return {
				...state,
				selectedEmailTemplate: {},
				emailTemplates: updatedEmailTemplates,
				actualEmailTemplates: updatedEmailTemplates
			};

		case "CHANGE_STATUS_EMAIL_TEMPLATE":
			const toggledEmailTemplates = state.emailTemplates.map(t =>
				t._id === action.payload._id ? action.payload : t
			);
			return {
				...state,
				emailTemplates: toggledEmailTemplates,
				actualEmailTemplates: toggledEmailTemplates
			};

		case "DELETE_EMAIL_TEMPLATE":
			const filteredEmailTemplates = state.emailTemplates.filter(t => t._id !== action.payload._id);
			const isRemovedEmailTemplate = state.selectedEmailTemplate._id === action.payload._id 
				? {} : state.selectedEmailTemplate;
			
			return {
				...state,
				selectedEmailTemplate: isRemovedEmailTemplate,
				emailTemplates: filteredEmailTemplates,
				actualEmailTemplates: filteredEmailTemplates
			};
				
		default:
			return state;
    }
}
  
import { compareValues } from '../helpers/Utils'; 

export default function reducer(state, action) {
    switch (action.type) {
		case "GET_TODOS":
			return {
				...state,
				todos: action.payload,
				actualTodos: action.payload
			};

		case "ADD_TODO":
			const addedTodos = [action.payload, ...state.todos];

			return {
				...state,
				todos: addedTodos,
				actualTodos: addedTodos
			};

		case "BULK_ADD_TODOS":
			const bulkAddedTodos = [...action.payload, ...state.todos];

			return {
				...state,
				todos: bulkAddedTodos,
				actualTodos: bulkAddedTodos
			};

		case "SET_SELECTED_TODO":
			return {
				...state,
				selectedTodo: action.payload
			};
			  
		case "UPDATE_TODO":
			const updatedTodo = { ...action.payload };
			const updatedTodoIndex = state.todos.findIndex(
				t => t._id === state.selectedTodo._id
			);
			const updatedTodos = [
				...state.todos.slice(0, updatedTodoIndex),
				updatedTodo,
				...state.todos.slice(updatedTodoIndex + 1)
			];
			
			return {
				...state,
				selectedTodo: {},
				todos: updatedTodos,
				actualTodos: updatedTodos
			};

		case "CHANGE_STATUS_TODO":
			const toggledTodos = state.todos.map(t =>
				t._id === action.payload._id ? action.payload : t
			);
			return {
				...state,
				todos: toggledTodos,
				actualTodos: toggledTodos
			};

		case "DELETE_TODO":
			const filteredTodos = state.todos.filter(t => t._id !== action.payload._id);
			const isRemovedTodo = state.selectedTodo._id === action.payload._id 
				? {} : state.selectedTodo;
			
			return {
				...state,
				selectedTodo: isRemovedTodo,
				todos: filteredTodos,
				actualTodos: filteredTodos
			};

		case "FILTER_TODOS":
			const searchFilter = action.payload;

			if (searchFilter) {
				const result = state.actualTodos.filter(item => item.status == searchFilter);
				return {
					...state,
					todos: result || []
				};
			} else {
				return {
					...state,
					todos: [...state.actualTodos]
				};
			}
			
		case "SEARCH_TODOS":
			const query = action.payload;

			if (query) {
				const result = state.actualTodos.filter(item => item.name.toLowerCase().includes(query.toLowerCase()));
				return {
					...state,
					todos: result || []
				};
			} else {
				return {
					...state,
					todos: [...state.actualTodos]
				};
			}

		case "SORT_TODOS":
			const sortedData = [...state.actualTodos];
			const result = sortedData.sort(compareValues(action.payload.sortKey, action.payload.sortType));        
		
			return {
				...state,
				todos: result
			};

		case "RESET_SORT_TODOS":
			return {
				...state,
				todos: action.payload
			};
				
		default:
			return state;
    }
}
  
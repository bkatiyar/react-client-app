import React, { useContext, useReducer, useEffect, Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import UserContext from '../context/UserContext';
import UserReducer from '../reducers/UserReducer';
import { useAPI } from '../helpers/Utils';
import PrivateRoute from './PrivateRoute'
import Header from '../components/common/Header';
import Footer from '../components/common/Footer';
import Dashboard from '../components/dashboard/Dashboard';
import Analytics from '../components/analytics/Analytics';
import Todo from '../components/todo/Todo';
import Template from '../components/template/Template';
import Sequence from '../components/sequence/Sequence';
import Login from '../components/user/Login';
import Register from '../components/user/Register';
import Profile from '../components/user/Profile';

const AppRoutes = () => {
	const initialState = useContext(UserContext);
	const [state, dispatch] = useReducer(UserReducer, initialState);
	const currentUser = useAPI("users/details");

	useEffect(
		() => {
			dispatch({
				type: "SET_CURRENT_USER",
				payload: currentUser
			});
		},
		[currentUser]
	);

	return (
		<div className="container">      
			<BrowserRouter>
				<UserContext.Provider value={{ state, dispatch, currentUser }}>
					<Suspense fallback={null}>
						<Header />							
						<div className="jumbotron">
							<Switch>
								{ /* Independent routes: for non logged-in user */ }
								<Route exact path="/" component={Dashboard} />															
								<Route path="/login" component={Login} />
								<Route path="/register" component={Register} />

								{ /* Dependent routes: for logged-in user */ }
								<PrivateRoute path="/analytics" component={Analytics} currentUser={currentUser} />
								<PrivateRoute path="/template" component={Template} currentUser={currentUser} />
								<PrivateRoute path="/sequence" component={Sequence} currentUser={currentUser} />
								<PrivateRoute path="/todo" component={Todo} currentUser={currentUser} />	
								<PrivateRoute path="/profile" component={Profile} currentUser={currentUser} />
							</Switch>
						</div>
						<Footer />
					</Suspense>	
				</UserContext.Provider>
			</BrowserRouter>
		</div>
	);
}

export default AppRoutes;

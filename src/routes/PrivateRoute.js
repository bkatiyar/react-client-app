import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, currentUser, ...rest }) => {
	console.log(currentUser);
	const isLoggedIn = currentUser ? true : (currentUser === undefined 
		|| !currentUser.length) ? true : false;

	return (
		<Route
			{...rest}
			render={props =>
			isLoggedIn ? (
				<Component {...props} />
			) : (
				<Redirect to={{ pathname: '/', state: { from: props.location } }} />
			)
			}
		/>
	)
};

export default PrivateRoute;